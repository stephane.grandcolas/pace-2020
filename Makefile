
DEBUG=no

ifeq ($(DEBUG),yes)
   CFLAGS= -I sga -g -Wall -pedantic -std=c99
else
   CFLAGS= -I sga -std=c99 -O3
endif


ifeq ($(PACE),yes)
   PACE_FLAG= -DPACE_2020
endif

src=./src

all: daia


daia: obj/main.o obj/lists.o obj/lire.o  obj/graph.o obj/tree.o obj/separator.o obj/decompose.o obj/heap.o obj/sets.o obj/utils.o obj/components.o obj/improve.o
	gcc $(CFLAGS) obj/main.o obj/lists.o obj/lire.o obj/graph.o  obj/tree.o obj/separator.o obj/decompose.o obj/heap.o obj/sets.o obj/utils.o  obj/components.o obj/improve.o -o daia

obj/improve.o: ${src}/improve.c ${src}/improve.h ${src}/graph.h ${src}/utils.h ${src}/sets.h ${src}/lists.h ${src}/tree.h ${src}/decompose.h ${src}/main.h 
	gcc $(CFLAGS) -c ${src}/improve.c -o obj/improve.o
	

obj/components.o: ${src}/components.c ${src}/components.h ${src}/graph.h ${src}/utils.h ${src}/sets.h ${src}/lists.h ${src}/heap.h
	gcc $(CFLAGS) -c ${src}/components.c -o obj/components.o

obj/utils.o: ${src}/utils.c ${src}/utils.h
	gcc $(CFLAGS) -c ${src}/utils.c -o obj/utils.o

obj/sets.o: ${src}/sets.c ${src}/sets.h 
	gcc $(CFLAGS) -c ${src}/sets.c -o obj/sets.o

obj/decompose.o: ${src}/decompose.c ${src}/decompose.h ${src}/separator.h ${src}/graph.h ${src}/utils.h ${src}/tree.h  ${src}/separator.h ${src}/heap.h ${src}/main.h ${src}/components.h ${src}/improve.h 
	gcc $(CFLAGS) $(PACE_FLAG) -c ${src}/decompose.c -o obj/decompose.o

obj/separator.o: ${src}/separator.c ${src}/separator.h ${src}/graph.h ${src}/utils.h ${src}/components.h ${src}/main.h ${src}/heap.h
	gcc $(CFLAGS) -c ${src}/separator.c -o obj/separator.o

obj/tree.o: ${src}/tree.c ${src}/tree.h ${src}/graph.h ${src}/utils.h ${src}/sets.h ${src}/decompose.h 
	gcc $(CFLAGS) -c ${src}/tree.c -o obj/tree.o

obj/main.o: ${src}/main.c ${src}/lists.h ${src}/lire.h ${src}/graph.h ${src}/main.h 
	gcc $(CFLAGS) $(PACE_FLAG) -c ${src}/main.c -o obj/main.o

obj/graph.o: ${src}/graph.c ${src}/graph.h ${src}/lists.h
	gcc $(CFLAGS) -c ${src}/graph.c -o obj/graph.o

obj/lists.o: ${src}/lists.c ${src}/lists.h 
	gcc $(CFLAGS) -c ${src}/lists.c -o obj/lists.o

obj/lire.o: ${src}/lire.c ${src}/lire.h ${src}/lists.h ${src}/graph.h ${src}/main.h
	gcc $(CFLAGS) -c ${src}/lire.c -o obj/lire.o


obj/heap.o: ${src}/heap.c ${src}/heap.h
	gcc $(CFLAGS) -c ${src}/heap.c -o obj/heap.o 

clean:
	rm -f sga \
	rm -f obj/*.o


