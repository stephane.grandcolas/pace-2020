//
// Created by Stephane on 10/03/2020.
//

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "decompose.h"
#include "graph.h"
#include "tree.h"
#include "sets.h"
#include "utils.h"




Node newNode(int vertex, Graph g) {
    Node v = malloc(sizeof(struct node));
    v->vertex = vertex;
    v->height = 0;
    v->nbhmax = 0;
    v->next = v->prev = v->fbs = v->lbs = NULL;
    v->father = NULL;
    v->nodes = NULL; //allocSet(g->n);
    v->depth = 0;
    v->date = 0;
    return v;
}


void resetNode(Node p) {
    p->fbs = p->lbs = p->next = p->prev = p->father = NULL;
    p->height = 1;
    p->nbhmax = 0;
    //VideSet(p->nodes);
}


// Calculate the height for this node, returns true if height has been modified
int initializeHeight(Node p) {
    int h = p->height;
    p->height = 1;
    Node q = p->fbs;
    p->nbhmax = 0;
    while (q != NULL) {
        if (p->height < q->height+1) { p->height = q->height+1; p->nbhmax = 1; }
        else if (p->height == q->height+1) p->nbhmax ++;
        q = q->next;
    }
    return (p->height != h);
}


// Update heights and number of children with max height for all the nodes
void updateHeights(Node root) {
    Node q = root->fbs;
    while (q != NULL) {
        updateHeights(q);
        q = q->next;
    }
    initializeHeight(root);
}

// Update depths
void updateDepths(Node p, int depth) {
    p->depth = depth;
    Node q = p->fbs;
    while (q != NULL) {
        updateDepths(q, depth + 1);
        q = q->next;
    }
}

// prepare improvement: set depths and heights and free nodes sets for all nodes of the subtree
void updateDepthsAndHeights(Node p, int depth, int verbose) {
    int height = 1;

    if (verbose && (p->depth != depth)) printf("depth node %d : %d -> %d\n", p->vertex, p->depth, depth);
    p->depth = depth;

    Node q = p->fbs;
    while (q != NULL) {
        updateDepthsAndHeights(q, depth + 1, verbose);
        if (q->height+1 > height) height = q->height+1;
        q = q->next;
    }

    if (verbose && (p->height != height)) printf("height node %d : %d -> %d\n", p->vertex, p->height, height);
    p->height = height;

}


int nbNeighborsAbove(Node node, Graph g) {
    int *N = g->slists[node->vertex];
    int nN = g->nadj[node->vertex];
    int nb = 0;

    Node p = node->father;
    while (p != NULL) {
        if (isInSet(p->vertex, NULL, N, nN))
            nb ++;
        p = p->father;
    }
    return nb;
}

// Returns the leftmost deepest node
Node leftmostDeepestNode(Node root) {
    Node p = root;
    while (1) {
        Node q = p->fbs;
        while (q != NULL) {
            if (q->height+1 == p->height)
                break;
            q = q->next;
        }
        if (q == NULL) return p;
        p = q;
    }
}


// the first ancestor whose father is marked with date (that is which is a node of the critical branch)
Node criticalAncestor(Node p, int date) {
    while (p != NULL) {
        if (p->father->date == date) break;
        p = p->father;
    }
    return p;
}



// Count at each level the number of critical nodes
void exploreMakeStats(Node node, int depth, int nbCritical[], int *max) {
    Node q = node->fbs;
    while (q != NULL) {
        if (q->height+1 == node->height)
            exploreMakeStats(q, depth+1, nbCritical, max);
        q = q->next;
    }
    nbCritical[depth] ++;
    if (*max < depth) *max = depth;
}

int makeStatsCriticalNodes(Node node, int nbCritical[]) {
    int max = 0;
    for (int i = 0; i < node->height; i ++)
        nbCritical[i] = 0;
    exploreMakeStats(node, 0, nbCritical, &max);
    return max;
}



// update height after insertion of a new child or increase of the height of a child
int incHeightNodeUpdate(Node node, int height) { // node gets height among its children
    if (node->height < height+1) {
        node->height = height+1;
        node->nbhmax = 1;
        return 1;
    }
    if (node->height == height+1) node->nbhmax ++;
    return 0;
}




// New child or child height increase
void updateIncHeightOnBranch(Node p, int height) {
    while (p != NULL) {
        if ( ! incHeightNodeUpdate(p, height))
            break;
        p = p->father;
        height ++;
    }
}


// insert node at last position in the list of children
void addChild(Node p, Node father) {

    p->next = NULL;
    p->father = father;

    if (father->fbs == NULL) {
        father->fbs = father->lbs = p;
        p->prev = NULL;
    }
    else {
        father->lbs->next = p;
        p->prev = father->lbs;
        father->lbs = p;
    }
    // updateIncHeightOnBranch(father, p->height); abandoned, it is better to update only once
    incHeightNodeUpdate(father, p->height);
}




// Suppose father exists !!
void justRemoveChild(Node node) {
    Node father = node->father;
    if (node == father->fbs)
        father->fbs = node->next;
    else {
        Node p = father->fbs;
        while (p->next != node)
            p = p->next;
        p->next = node->next;
    }
}



Node bottomNode;

Node makeSingleBranch(int S[], int n, int nbNInAB[], Graph g) {
    // put at bottom those which have the fewer neighbors in A and B

    if (1 && nbNInAB != NULL)
        QSort1(S, nbNInAB, 0, n-1);

    if (n == 0)
        return NULL;

    Node node, father;

    bottomNode = node = theNodes[S[0]];
    resetNode(bottomNode);

    for (int i = 1; i < n; i ++) {
        father = theNodes[S[i]];
        resetNode(father);
        addChild(node, father);
        node = father;
    }

    return node;
}









//
// Sets of nodes (for swap in critical branches)
//


void makeAllSetsOfNodes(Node node, Graph g) {
    Node q = node->fbs;
    while (q != NULL) {
        makeAllSetsOfNodes(q, g);
        q = q->next;
    }
    if (node->nodes == NULL) node->nodes = allocSet(g->n);
    else clearSet(node->nodes);
    q = node->fbs;
    while (q != NULL) {
        addSet(q->nodes, node->nodes);
        q = q->next;
    }
}


// Construct sets of nodes for a given node, and for its children, suppose children sets do not exist
void exploreAndBuildSetOfNodes(Node p, SET V) {
    ADDe(p->vertex, V);
    Node q = p->fbs;
    while (q != NULL) {
        exploreAndBuildSetOfNodes(q, V);
        q = q->next;
    }
}



void freeSetsOfNodes(Node nodes[], int nb) {

    for (int i = 0; i < nb; i ++) {
        free(nodes[i]->nodes);
        nodes[i]->nodes = NULL;
    }
}



//
// Verifications
//


int verifyNode(Node root) {

    if (root == NULL)
        return NONE;

    int height = 1;
    Node p = root->fbs;

    while (p != NULL) {
        if (p->height+1 > height)
            height = p->height+1;
        p = p->next;
    }

    if (root->height != height)
        return height;
    return NONE;
}

// Verify heights in nodes
int verifyTree(Node root, int depth) {
    int height;

    if (root == NULL)
        return 1;

    Node p = root->fbs;
    int vertex;
    if (p != NULL)
        vertex = p->vertex;

    while (p != NULL) {
        if ( ! verifyTree(p, depth+1))
            return 0;
        p = p->next;
        if ((p != NULL) && (p->vertex == vertex)) {
            printf("children list loops !!\n");
            exit(0);
        }
    }

    height = verifyNode(root);
    if (height != NONE) {
        printf("bad height node %d at depth %d : height is %d should be %d\n", root->vertex, depth, root->height, height);
        return 0;
    }

    return 1;
}


void exploreAndPrint(Node node, int depth) {
    for (int i = 0; i < depth; i ++) printf(" -");
    printf("[%d:%dx%d]\n", node->vertex, node->height, node->nbhmax);
    Node p = node->fbs;
    if (p == NULL)
        return;
    //printf("(");
    while (p != NULL) {
        exploreAndPrint(p, depth+1);
        p = p->next;
        if (p != NULL) printf(",");
    }
    //printf(")");
}

void printTree(Node node) {
    exploreAndPrint(node, 0);
    printf("\n");
}


int sizeForest(Node node) {
    int nb = 0;
    while (node != NULL) {
        nb += 1+ sizeForest(node->fbs);
        node = node->next;
    }
    return nb;
}


int sizeTree(Node node) {
    if (node == NULL) return 0;
    int nb = 1;
    Node p = node->fbs;
    while (p != NULL) {
        nb += sizeTree(p);
        p = p->next;
    }
    return nb;
}



int treeContains(int e, Node p) {
    if (p == NULL) return 0;
    if (p->vertex == e) return 1;
    for (Node q = p->fbs; q != NULL; q = q->next)
        if (treeContains(e, q))
            return 1;
    return 0;
}












