//
// Created by Stephane on 08/04/2020.
//

#ifndef SGA_CRITICAL_PATH_H
#define SGA_CRITICAL_PATH_H

#include "graph.h"
#include "tree.h"
#include "sets.h"




extern int nbCallsEject;

Node makeEjectionsInCriticalBranch(Node root, Graph g);
int listEjections(Node root, int date, int maxEjections, int maxSearches, Graph g);
Node makeEjections(Node root, int nbEjections, Graph g);
int selectEjections(int *nodes, int *corr, int nb, int maxLen);
Node ejectNode(Node root, Node node, Node cc, Graph g);
int evaluateSwap(Node node, Node cc, Graph g);
Node markCriticalBranch(Node root, int date, int single, Graph g);
int verifyCriticalBranch(Node root, Node cc, int date, Graph g);
Node searchCriticalCorrespondant(Node node, int *maxHeight, int date, int store, Graph g);

#endif //SGA_CRITICAL_PATH_H
