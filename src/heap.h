
#ifndef _H_tas
#define _H_tas

/*
 *  tas.h
 *  packing-satmod
 *
 *  Created by St�phane on 11/11/14.
 *  Copyright 2014 __MyCompanyName__. All rights reserved.
 *
 */


typedef struct heap
{
    int size; /* Allocated size. */
    int n; /* Number of values in the heap. */
    int *val;
    int *ind; /* Indexes of the values in val[]. */
    int (*lt)(int, int);
    int min; // for a certain evaluation, the minimal value for the elements of the heap
    int nbmin; // the number of elements in the heap that have for evaluation the value min
}
        * Heap;



#define FG(i) ((i)*2+1)
#define FD(i) ((i)*2+2)
#define PERE(i) (((i)-1)/2)



Heap allocHeap (int size, int (*lt)(int, int));
void freeHeap(Heap F);
void heapSetCompare(Heap F, int (*lt)(int, int));
void resetHeap(Heap F);
void heapBubbleDown(Heap F, int i);
void makeHeap(Heap F);
int heapExtractMin (Heap F);
void heapBubbleUp (int i, Heap F);
void heapRemove (int x, Heap F);
void heapJustRemove (int x, Heap F);
void heapRemoveAll(Heap heap);
void heapDecreaseKey (int x, Heap F);
void heapInsert (int x, Heap F);
void heapJustAdd (int x, Heap F);
void heapSwap (int i, int j, Heap F);
void heapPrint(Heap F, int *weight, int n, int infini);
void heapVerif(Heap F, int *w);
void heapPrintMax(Heap F, int *weight);
int isInHeap(int x, Heap F);

// minheaps
void minheapUpdateMin(Heap F, int bound, int val[]);
void minheapSearchAndPackMins(Heap F, int val[]);
void minheapPackMins(Heap F, int val[]);
void minheapRemove(int x, Heap F, int val[]);
void minheapJustRemove(int x, Heap F, int val[]);
void minheapDecreaseValue(int x, Heap F, int val[]);


#endif








