
#ifndef _H_lire
#define _H_lire

/*
 *  lire.h
 *  graphes
 *
 *  Created by St�phane on 12/02/18.
 *  Copyright 2018 __MyCompanyName__. All rights reserved.
 *
 */

#include <stdio.h>

#include "graph.h"


#define STANDARD 0
#define STEINER_TREES_PACE_2018 1

int countGraphe(FILE *f);

Graph lireGraphe(FILE *f);







#endif



