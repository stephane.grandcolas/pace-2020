#ifndef _H_sets
#define _H_sets

#include <stdio.h>
#include <stdlib.h>

/* Les deux segmets initiaux pour stocker le cardinal et la taille. */
#define SEGMARGIN 2


typedef unsigned int TYPESEG; /* Type d'un segment. */
typedef TYPESEG *SET; /* Un ensemble. */

extern int Cardinal[];

/*
 * Prototypes.
 */

SET allocSet(int);
void copySet(SET, SET);
SET makeSet(int *S, int nb, SET E);
int nCommuns(SET , SET );
int nFaitsCommunsInSet(SET s1, SET s2);
void freeSet(SET);
char setsIntersect(SET, SET);
void addSet(SET, SET);
int isSubSet(SET, SET);
int isSupSet(SET, SET);
void clearSet(SET);
void FillSet(SET s, int n);
void FactsLevelToSet(int * F, int nf, SET S);


/*
 * Macros.
 */

#define SIZESEG 32

#define CARD(s) *(s)
#define TAILLE(s) *((s)+1) /* Nombre de segments. */

#define IS_EMPTY(s) (*(s) == 0)

#define IN(e,s) ((*((s)+(e)/SIZESEG+SEGMARGIN)&((TYPESEG) 1)<<((e)%SIZESEG)) != ((TYPESEG) 0))
#define ADDe(e,s) if (!IN((e),(s))) {*((s)+(e)/SIZESEG+SEGMARGIN) |= (TYPESEG) (((TYPESEG) 1)<<((e)%SIZESEG)); (CARD(s))++;}
#define RETe(e,s) if (IN((e),(s))) {*((s)+(e)/SIZESEG+SEGMARGIN) ^= ((TYPESEG) 1)<<((e)%SIZESEG); (CARD(s))--;}
#define CALCARD(s) {\
  int iCALCARD,nCALCARD;\
  unsigned char *pCALCARD;\
  for (iCALCARD=0, pCALCARD=(unsigned char *) ((s)+SEGMARGIN), nCALCARD=0; iCALCARD < TAILLE(s); pCALCARD+=4, iCALCARD++)\
     nCALCARD+=Cardinal[*pCALCARD]+Cardinal[*(pCALCARD+1)]+Cardinal[*(pCALCARD+2)]+Cardinal[*(pCALCARD+3)];\
  CARD(s) = nCALCARD;\
}

#define ADDs(s1,s2) {\
 int iADDs;\
 for (iADDs=SEGMARGIN; iADDs < TAILLE(s2)+SEGMARGIN; iADDs++)\
 *((s2)+iADDs) |= *((s1)+iADDs);\
 CALCARD(s2)\
 }
#define RETs(s1,s2) {\
 int iRETs;\
 for (iRETs=SEGMARGIN; iRETs < TAILLE(s2)+SEGMARGIN; iRETs++)\
 *((s2)+iRETs) &= ~*((s1)+iRETs);\
 CALCARD(s2)\
 }
#define INTERs(s1,s2) {\
    int iINTERs;					\
    for (iINTERs=SEGMARGIN; iINTERs < TAILLE(s2)+SEGMARGIN; iINTERs++)	\
      *((s2)+iINTERs) &= *((s1)+iINTERs);		\
    CALCARD(s2)						\
      }

#define AFFSET(s) {\
 int iAFFSET;\
 printf("[%ld/%ld]  ",CARD(s),TAILLE(s));\
 for (iAFFSET=0; iAFFSET<TAILLE(s)*SIZESEG; iAFFSET++)\
 if IN(iAFFSET,s)\
 printf("%5ld",iAFFSET);\
 printf("\n");\
 }
#endif
