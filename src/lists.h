#ifndef _H_lists
#define _H_lists

/*
 *  lists.h
 *  graphes
 *
 *  Created by St�phane on 12/02/18.
 *  Copyright 2018 __MyCompanyName__. All rights reserved.
 *
 */


typedef struct maillon * LIST;

struct maillon {
      int val;
      int w;
      LIST suiv;
};



LIST newLNode(int v, LIST s, int w);


#endif


