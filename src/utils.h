//
// Created by Stephane on 10/03/2020.
//

#ifndef SRC_UTILS_H
#define SRC_UTILS_H

#include "sets.h"

#define NONE (-1)

int chooseWeightedRandom(int weights[], int S[], int n, int total);
void copyList(int *dest, int *src, int n);
void printList(int S[], int n);
void shakeList(int S[], int pos[], int n, int nSteps);
int nbOccs(int x, int S[], int n);
int maxInList(int S[], int n);
int indexMaxInList(int S[], int n);
int extractList(int num, int S[], int n, int I[], int T[]);
int packListLeft(int num, int S[], int n, int C[]);
void copyListLeft(int S[], int start, int size);
int nbInListWithThisValue(int num, int *S, int n, int *values);

int isInSet(int v, SET S, int T[], int n);
int isInList(int v, int T[], int n);

void swap(int arr[], int i, int j);
void swapValues(int *p, int *q);
void Introsort(int arr[], int *begin, int *end);
void InsertionSort(int S[], int a, int b);
void QSort1(int *T, int *V, int Deb, int Fin);


#endif //SRC_UTILS_H
