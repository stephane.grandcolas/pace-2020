//
// Created by Stephane on 10/03/2020.
//

#ifndef SRC_TREE_H
#define SRC_TREE_H

#include "graph.h"
#include "sets.h"

typedef struct node * Node;
struct node {
    int vertex;
    Node fbs; // first child
    Node lbs; // last child
    Node next;
    Node prev;
    Node father;
    //int size;
    int height;
    int nbhmax; // the number of children with max height
    SET nodes;
    int depth;
    int date; // for swaps, to mark nodes of the critical branch at this moment
};



extern Node bottomNode;


Node newNode(int vertex, Graph g);
void resetNode(Node p);

int initializeHeight(Node p);
void updateHeights(Node root);
void updateDepths(Node p, int depth);
void updateDepthsAndHeights(Node p, int depth, int verbose);
int nbNeighborsAbove(Node p, Graph g);
Node leftmostDeepestNode(Node root);
Node criticalAncestor(Node p, int date);
int makeStatsCriticalNodes(Node node, int nbCritical[]);

void updateIncHeightOnBranch(Node p, int height);
int incHeightNodeUpdate(Node node, int height);

void addChild(Node p, Node father);
void justRemoveChild(Node node);

Node makeSingleBranch(int S[], int n, int nbNInAB[], Graph g);


void makeAllSetsOfNodes(Node node, Graph g);
void exploreAndBuildSetOfNodes(Node p, SET V);

void freeSetsOfNodes(Node nodes[], int nb);

int verifyTree(Node root, int depth);

void printTree(Node node);
int sizeForest(Node node);
int sizeTree(Node node);

int treeContains(int e, Node p);

#endif //SRC_TREE_H
